#!/bin/sh
docker run \
	--name html-testing-tool-frontend-nginx \
	-e "VIRTUAL_HOST=html-testing-tool.shneky.com" \
    -e "LETSENCRYPT_HOST=html-testing-tool.shneky.com" \
    -e "LETSENCRYPT_EMAIL=shneky@protonmail.com" \
	--network nginx \
	-v html-testing-tool-frontend:/usr/share/nginx/html:ro \
	-d nginx:alpine
