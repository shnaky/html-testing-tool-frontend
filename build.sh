#!/bin/sh


APP_NAME=html-testing-tool-frontend

docker volume create $APP_NAME

docker build -t $APP_NAME .

docker run --rm \
	--name $APP_NAME \
	-v $APP_NAME:/volume \
	$APP_NAME
