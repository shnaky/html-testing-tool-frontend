import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { TestWindowComponent } from './test-window/test-window.component';
import { CodeWindowComponent } from './test-window/code-window/code-window.component';

const dashboardRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent, children: [
    { path: 'test/:id', component: TestWindowComponent, children: [
      { path: 'code/:id', component: CodeWindowComponent }
    ]}
  ]},
];

@NgModule({
  imports: [
    RouterModule.forChild(dashboardRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule {}
