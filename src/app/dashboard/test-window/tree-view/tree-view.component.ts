import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';

interface HtmlElement {
  index: number;
  attrId: string;
  xpath: string;
}

export class HtmlNode {
  children: HtmlNode[];
  tag: string;
}

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent implements OnInit {
  nestedTreeControl: NestedTreeControl<HtmlNode>;
  nestedDataSource: MatTreeNestedDataSource<HtmlNode>;
  dataChange: BehaviorSubject<HtmlNode[]> = new BehaviorSubject<HtmlNode[]>([]);

  constructor() {
    this.nestedTreeControl = new NestedTreeControl<HtmlNode>(this.getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

    this.dataChange.subscribe(data => this.nestedDataSource.data = data);

    this.dataChange.next([
      {
        tag: 'div',
        children: [
          {
            tag: 'h1',
            children: []
          },
          {
            tag: 'ul',
            children: [
              {
                tag: 'li',
                children: [
                  {
                    tag: 'a',
                    children: []
                  }
                ]
              },
              {
                tag: 'li',
                children: [
                  {
                    tag: 'a',
                    children: []
                  }
                ]
              },
              {
                tag: 'li',
                children: [
                  {
                    tag: 'a',
                    children: []
                  }
                ]
              },
            ]
          }
        ]
      }
    ]);
  }

  getChildren = (node: HtmlNode) => observableOf(node.children);

  hasNestedChild = (_: number, nodeData: HtmlNode) => nodeData.children.length > 0;

  ngOnInit() {
  }


}
