import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

class TestWindow {
  constructor(private id: number, private label: string) {
  }
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  testWindows = [new TestWindow (1, 'one'), new TestWindow(2, 'two')];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
    });
  }

}
