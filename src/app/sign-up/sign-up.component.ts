import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  password = '';
  passwordRepeat = '';
  hide = true;

  constructor() { }

  onSubmit() {
    if (this.email.valid) {
      console.log('submit');
    }
  }

  getErrorMsg(): string {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid Email' : '';
  }

  ngOnInit() {
  }

}
